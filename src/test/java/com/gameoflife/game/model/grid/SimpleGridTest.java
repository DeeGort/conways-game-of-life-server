package com.gameoflife.game.model.grid;

import com.gameoflife.game.model.cell.Cell;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class SimpleGridTest {

    private SimpleGrid simpleGrid;

    @Before
    public void init() {
        simpleGrid = new SimpleGrid();
    }

    @Test
    public void insert() {
        simpleGrid.insert(0, 0);
        simpleGrid.insert(0, 1);
        simpleGrid.insert(1, 1);
        simpleGrid.insert(-8, 15);
    }

    @Test
    public void get() {
        simpleGrid.insert(0, 0);
        simpleGrid.insert(-8, 75);

        assertEquals(simpleGrid.get(0, 0), true);
        assertEquals(simpleGrid.get(-8,  75), true);
        assertEquals(simpleGrid.get(-1,  -2), false);
        assertEquals(simpleGrid.get(-123,  321), false);
    }

    @Test
    public void iteration() {
        simpleGrid.insert(0, 0);
        simpleGrid.insert(-8, 75);
        simpleGrid.insert(7, 11);

        ArrayList<Cell> livingCellPositions = new ArrayList<Cell>();
        for (Cell c : simpleGrid) {
            livingCellPositions.add(c);
        }

        assertEquals(livingCellPositions.size(), 3);
    }

}