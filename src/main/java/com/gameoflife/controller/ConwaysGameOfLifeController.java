package com.gameoflife.controller;

import com.gameoflife.game.model.cell.Cell;
import com.gameoflife.game.model.grid.Grid;
import com.gameoflife.game.model.grid.SimpleGrid;
import com.gameoflife.game.model.rule.Rule;
import com.gameoflife.game.service.GameOfLife;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ConwaysGameOfLifeController {

    private GameOfLife gameOfLife;

    public ConwaysGameOfLifeController(GameOfLife gameOfLife) {
        this.gameOfLife = gameOfLife;
    }

    @RequestMapping("/next")
    public Grid [] nextGeneration(
            @RequestBody List<Cell> seedCells,
            @RequestParam(value="generation", defaultValue="1") int generation,
            @RequestParam(value="survival", defaultValue="23") String survival,
            @RequestParam(value="birth", defaultValue="3") String birth
    ) {
        Rule rule = new Rule(survival, birth);

        // Generation store
        Grid [] grids = new Grid[generation];

        // Init zero generation
        Grid zero = new SimpleGrid();
        seedCells.forEach(cell -> zero.insert(cell.getX(), cell.getY()));

        // Generate generations
        grids[0] = gameOfLife.next(zero, rule);
        for (int i = 1; i < generation; i++) {
            grids[i] = gameOfLife.next(grids[i-1], rule);
        }

        return grids;
    }

}
