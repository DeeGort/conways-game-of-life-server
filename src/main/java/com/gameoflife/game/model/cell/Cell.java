package com.gameoflife.game.model.cell;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Arrays;
import java.util.List;

public class Cell {
    private int x;
    private int y;

    public Cell() {

    }

    public Cell(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @JsonIgnore
    public List<Cell> getNeighbours() {
        return Arrays.asList(
                new Cell(x - 1, y - 1),
                new Cell(x, y - 1),
                new Cell(x + 1, y - 1),
                new Cell(x - 1, y),
                new Cell(x + 1, y),
                new Cell(x - 1, y + 1),
                new Cell(x, y + 1),
                new Cell(x + 1, y + 1)
        );
    }
}
