package com.gameoflife.game.model.rule;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Rule {
    private List<Integer> survival;
    private List<Integer> birth;

    public Rule(List<Integer> survival, List<Integer> birth) {
        this.survival = survival;
        this.birth = birth;
    }

    public Rule(String survival, String birth) {
        this.survival = Stream.of(survival.split(""))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
         this.birth = Stream.of(birth.split(""))
                 .map(Integer::parseInt)
                 .collect(Collectors.toList());
    }

    public List<Integer> getSurvival() {
        return survival;
    }

    public void setSurvival(List<Integer> survival) {
        this.survival = survival;
    }

    public List<Integer> getBirth() {
        return birth;
    }

    public void setBirth(List<Integer> birth) {
        this.birth = birth;
    }
}
