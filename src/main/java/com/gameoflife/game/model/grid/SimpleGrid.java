package com.gameoflife.game.model.grid;

import com.gameoflife.game.model.cell.Cell;

import java.util.*;

public class SimpleGrid implements Grid {
    private Map<Integer, Map<Integer, Boolean>> grid;

    public SimpleGrid() {
        grid = new HashMap();
    }

    @Override
    public void insert(int x, int y) {
        // If it is not initialized
        grid.computeIfAbsent(x, k -> new HashMap<>());

        grid.get(x).put(y, true);
    }

    @Override
    public boolean get(int x, int y) {
        // If it is not initialized
        if (grid.get(x) == null) return false;
        if (grid.get(x).get(y) == null) return false;

        return grid.get(x).get(y);
    }

    @Override
    public Iterator<Cell> iterator() {
        return flatten().iterator();
    }

    private List flatten() {
        List livingCellsList = new ArrayList<Cell>();
        for (Map.Entry<Integer, Map<Integer, Boolean>> x : grid.entrySet()) {
            for (Map.Entry<Integer, Boolean> y : x.getValue().entrySet()) {
                livingCellsList.add(new Cell(x.getKey(), y.getKey()));
            }
        }

        return livingCellsList;
    }
}
