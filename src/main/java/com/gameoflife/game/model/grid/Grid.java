package com.gameoflife.game.model.grid;

import com.gameoflife.game.model.cell.Cell;

public interface Grid extends Iterable<Cell> {
    void insert(int x, int y);
    boolean get(int x, int y);
}
