package com.gameoflife.game.service;

import com.gameoflife.game.model.grid.Grid;
import com.gameoflife.game.model.rule.Rule;

public interface GameOfLife {
    Grid next(Grid grid, Rule rule);
}
