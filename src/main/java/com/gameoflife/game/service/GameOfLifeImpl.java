package com.gameoflife.game.service;

import com.gameoflife.game.model.cell.Cell;
import com.gameoflife.game.model.grid.Grid;
import com.gameoflife.game.model.grid.SimpleGrid;
import com.gameoflife.game.model.rule.Rule;
import org.springframework.stereotype.Service;

@Service
public class GameOfLifeImpl implements GameOfLife {

    @Override
    public Grid next(Grid grid, Rule rule) {
        Grid nextGrid = new SimpleGrid();

        for (Cell livingCell : grid) {
            for (Cell livingCellNeighbour : livingCell.getNeighbours())
                decide(livingCellNeighbour, grid, nextGrid, rule);
            decide(livingCell, grid, nextGrid, rule);
        }

        return nextGrid;
    }

    private void decide(Cell c, Grid grid, Grid nextGrid, Rule rule) {
        boolean isLiving = grid.get(c.getX(), c.getY());

        int livingNeighbours = (int)
            c.getNeighbours()
            .stream()
            .filter(neighbour -> grid.get(neighbour.getX(), neighbour.getY()))
            .count();

        if (isLiving && rule.getSurvival().contains(livingNeighbours) || rule.getBirth().contains(livingNeighbours)) {
            nextGrid.insert(c.getX(), c.getY());
        }

    }

}
